package Garage;
import java.util.*;

import robocode.*;

public class Situation 
{
	public long timeNow;
	public long timeSinceLastFire;
	public long timeSinceLastHit;
	
	public Vector2d lastRelativeLocation;
	public Vector2d relativeLocation;
	public Vector2d relativeLocationNextTurn;
	
	public double myVelocity;
	public double myEnergy;
	public double myHeading;
	
	
	public double theirVelocity;
	public double theirEnergy;
	public double theirHeading;
	public double theirBearing;
	
	Situation( )
	{
		myVelocity = Self.velocity;
		myEnergy = Self.energy;
		myHeading = Self.headingD;
		timeSinceLastFire = Self.time - Self.lastTimeFired;
		timeSinceLastHit = Self.time - Self.lastTimeHitTarget;
		
		if(Self.target != null)
		{
			theirVelocity = Self.target.velocity;
			theirEnergy = Self.target.energy;
			theirHeading = Self.target.headingD;
			theirBearing = Self.target.bearingD;
			
			relativeLocation = Self.target.relativeLocation;
			relativeLocationNextTurn = Self.target.predictedNextRelLoc;
		}
		
	}
	
}
