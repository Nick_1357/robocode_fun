package Garage;
//import robocode.*;

public class Node
{//specifically for a binary tree holding doubles
	
	public Node parent;
	public Node lesser;
	public Node greater;
	public int index;//organizing key
	public int depth;//root is zero depth
	public double dat;
	
	Node()
	{
		dat = 0.0;
		index = 0;
		depth = 0;
		parent = null;
		lesser = null;
		greater = null;
	}
	
	Node(double d, int mapping)
	{
		dat = d;
		depth = 0;
		index = mapping;
		parent = null;
		lesser = null;
		greater = null;
	}
	
	Node(Node n)//do not delete: ever
	{
		if(n==null)
		{
			dat = 0.0;
			index = 0;
			depth = 0;
			parent = null;
			lesser = null;
			greater = null;
		}
		else
		{
			dat = n.dat;
			index = n.index;
			depth = n.depth;
			parent = n.parent;
			lesser = n.lesser;
			greater = n.greater;
		}
	}
	
	Node(Node n, VecTree owner)
	{
		if(n==null)
		{
			dat = 0.0;
			index = 0;
			depth = 0;
			parent = null;
			lesser = null;
			greater = null;
		}
		else
		{
			dat = n.dat;
			index = n.index;
			depth = n.depth;
			parent = n.parent;
			owner.count++;
			
			if(n.lesser == null)lesser = null;
			else n.lesser = new Node(n.lesser,owner);
			if(n.greater == null)greater = null;
			else n.greater = new Node( n.greater, owner);
		}
	}
	
	public String toString()
	{
		return "||index:"+index+" depth:"+depth+"||,";//+"\ndata:"+dat;
	}
	
}//end of class
