package Garage;
import robocode.*;

public class Self 
{
	public static Other target = null;
	public static Vector2d location;
	public static Vector2d lastLocation;
	public static Vector2d futureLocation;
	public static Vector2d firingAt = new Vector2d(0,0);
	public static Bullet manyLikeIt = null;
	public static boolean haveTgt = true;
	public static boolean moving = false;
	public static int reverse = 1;
	public static Vector2d movingTo = new Vector2d(0,0);
	public static Vector2d relativeMovingTo = new Vector2d(0,0);
	public static long timeToNextFire;
	public static long lastTimeFired;
	public static long lastTimeHitTarget;
	
	public static double energy;
	
	public static double headingD;
	public static double gunHeadingD;
	public static double radarHeadingD;
	
	public static double velocity;
	
	public static double turnRemainingD;
	public static double radarTurnRemainingD;
	public static double gunTurnRemainingD;
	
	public static double distanceRemaining;
	public static double gunHeat;
	
	public static int others;
	
	public static long time = 0;
	public static double radarCorrection = 1;
	public static double gunCorrection = 1000;
	public static double bulletPower = 1;
	public static double arenaHeight;
	public static double arenaWidth;
	
	public Self()
	{}//empty default
	
	public static void updateSelf(RobotStatus r)
	{
		if(r == null)return;
		
		if(location == null)init();
		
		energy = r.getEnergy();//keeping track of own energy might save time on this function call. collisions,damage,firing change energy
			
		lastLocation.x = location.x;
		lastLocation.x = location.x;
		location.x = r.getX();
		location.y = r.getY();
			
		headingD = r.getHeading();
		gunHeadingD = r.getGunHeading();
		radarHeadingD = r.getRadarHeading();
			
		velocity = r.getVelocity();
			
		turnRemainingD = r.getTurnRemaining();
		radarTurnRemainingD = r.getRadarTurnRemaining();
		gunTurnRemainingD = r.getGunTurnRemaining();
			
		distanceRemaining = r.getDistanceRemaining();//might not need this
		gunHeat = r.getGunHeat();//same as energy. keeping track could save time with this function call
			
		others = r.getOthers();
		
		futureLocation = Vector2d.degToVector(headingD);
		futureLocation.x *= velocity;
		futureLocation.y *= velocity;
		futureLocation = futureLocation.plus(location);
		
	}
	
	public static void init()
	{
			location = new Vector2d(0,0);
			lastLocation = new Vector2d(0,0);
			futureLocation = new Vector2d(0,0);
			
			
	}
	
	
}
