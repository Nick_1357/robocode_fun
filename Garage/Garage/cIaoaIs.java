package Garage;
import robocode.*;
//import robocode.util.Utils;

//import java.util.*;
import java.awt.Color;
import java.awt.Graphics2D;

public class cIaoaIs extends AdvancedRobot 
{

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
	

	public void run() 
	{
		// Initialization of the robot should be put here
		
		Self.init();
		
		Self.arenaWidth = getBattleFieldWidth();
		Self.arenaHeight = getBattleFieldHeight();
		
		Vector2d arenaCenter = new Vector2d(Self.arenaWidth/2, Self.arenaHeight/2);
		Vector2d myPos = new Vector2d(getX(), getY());
		Vector2d myHead = Vector2d.degToVector(getHeading());

		setColors(Color.black,Color.black,Color.darkGray); // body,gun,radar

		
		// Robot main loop
		while(true) 
		{			
			//setTurnRadarRightRadians(Double.POSITIVE_INFINITY);
			//initial search
			while(Self.target == null)
			{
				Vector2d toCent = arenaCenter.minus(myPos);
				//find opponent
				
				//dot product of the heading.left vector with the toCenter vector
				if(toCent.x*myHead.y + toCent.y*-myHead.x > 0) //if center is to the left
				{
					
					setTurnRadarLeft(45);
					setTurnGunLeft(20);
					setTurnLeft(10);
				}
				else
				{
					setTurnRadarRight(45);
					setTurnGunRight(20);
					setTurnRight(10);
				}
				setAhead(-10);
				execute();
			}
			
			//free up the rotations
			setAdjustGunForRobotTurn(true);
			setAdjustRadarForGunTurn(true);
			setAdjustRadarForRobotTurn(true);
			
			if(Self.target != null)//there is a target to destroy
			{
				//don't lose lock-on
				double diff = Self.target.predictedNextLoc.minus(Self.location).getDirDegrees() - Self.radarHeadingD;
				if(diff >= 0.0)
				{
					if(diff >= 180)
					{
						diff = 360-diff;
					}
					else
					{
						diff = -diff;
					}
				}
				else
				{
					if(diff <= -180)
					{
						diff += 360;
					}
					else
					{
						diff = -diff;
					}
				}
				
				
			
				//diff is now the value the radar should turn to point straight at the target
				//now aim where it should be next turn
				
				Self.radarCorrection = Vector2d.radToDeg * Math.acos(Self.target.relativeLocation.getNormalized().dot(//the difference in angle between how it  
														   			 Self.target.predictedNextRelLoc.getNormalized()   ));//looks, and how the bot thinks next turn will look.
				
				int dirCor = (int)(Self.target.relativeLocation.getDirDegrees() - Self.target.predictedNextRelLoc.getDirDegrees());
				dirCor = (dirCor > 0)? ((dirCor<180)? 1:-1 ):((dirCor<-180)? 1:-1 );//basically: is it moving left or right?
				
				setTurnRadarLeft(diff + 1.5*(dirCor*Self.radarCorrection));
				if(diff + 1.5*(dirCor*Self.radarCorrection) == 0)scan();
				
				//reusing diff for the gun now
				diff = Self.firingAt.minus(Self.location).getDirDegrees() - Self.gunHeadingD;
				if(diff >= 0.0)
				{
					if(diff >= 180)
					{
						diff = 360-diff;
					}
					else
					{
						diff = -diff;
					}
				}
				else
				{
					if(diff <= -180)
					{
						//diff += 360;
					}
					else
					{
						diff = -diff;
					}
				}//////////////////////////////////////////////////////////////////////////
				
				
				
				setTurnGunLeft(diff);//****************************************************************
				
				
				if(Self.gunHeat == 0.0 && Math.abs(Self.firingAt.minus(Self.location).getDirDegrees() - Self.gunHeadingD ) < 35)
				{
					Self.manyLikeIt = setFireBullet(Self.bulletPower);
					
					//////////// now to improve ////////////
			
					//next chance to fire is gunHeat/cooling rate: cooling rate is 0.1/turn
					Self.timeToNextFire = (long)Self.gunHeat*10+1;///////////////error when gunHeat is an exact multiple of 0.1
					
					Self.lastTimeFired = Self.time;
					//predict location at time + next chance to fire
					
					
				}
					BehaviorPrediction.predictLocation(Self.timeToNextFire);
				
				//---------------------------------------- +move+ ------------------------------------------------
					
				//pick a random spot
					if(!Self.moving)
					{
						if(Self.target!= null) Self.movingTo = Self.target.location;
						Self.movingTo.x += (Math.random()-0.5)*144;//pick an area within a ~101 pixel radius circle around the target
						Self.movingTo.y += (Math.random()-0.5)*144;
						
						//Self.movingTo.x = Math.random()*Self.arenaWidth;//testing different movement strategies
						//Self.movingTo.y = Math.random()*Self.arenaHeight;
						
						if(Self.movingTo.x <18)Self.movingTo.x = 18;
						else if(Self.movingTo.x > (Self.arenaWidth-18))Self.movingTo.x = Self.arenaWidth-18;
						
						if(Self.movingTo.y <18)Self.movingTo.y = 18;
						else if(Self.movingTo.y > (Self.arenaHeight-18))Self.movingTo.y = Self.arenaHeight-18;
						
						Self.moving = true;
					}
					
					Self.relativeMovingTo = Self.movingTo.minus(Self.location);
					
					if(Self.relativeMovingTo.getMag() >40)
					{
						setAhead(Self.reverse*Self.relativeMovingTo.getMag() );
						setTurnRight((Self.relativeMovingTo.getDirDegrees()+((Self.reverse>0)? 0:180) - Self.headingD)  );//fix this------------------
					}
					else
					{
						Self.moving = false;
						//Self.reverse *= -1;
					}

				
				execute();
				
				if(Self.manyLikeIt != null)FireEvent.myHistory.addLast(new FireEvent(Self.time, Self.location, Self.firingAt, Self.manyLikeIt));
				
				if(Self.target!= null && Self.time - Self.target.timeLastScanned > 3 ) Self.target = null;//we lost them
				
			}
			

			
			
			
			Self.time++;
			Self.haveTgt = true;//boolean used to make sure the target is the closest picked up by the radar
			
		}
		
	}
	
	
	public void onPaint(Graphics2D g)
	{
		if(Self.target == null)return;
		g.setColor(new Color(0xff, 0x00, 0x00, 0x80));

		//g.drawLine((int)Self.target.predictedNextLoc.x, (int)Self.target.predictedNextLoc.y, (int)Self.location.x, (int)Self.location.y);
		
		g.fillRect((int)Self.target.location.x-20, (int)Self.target.location.y-20, 40, 40);
		
		g.setColor(new Color(0, 0, 0, 255));//black
		g.drawLine(40,90,140,90);//
		g.drawLine(90, 40, 90, 140);
		
		g.setColor(new Color(0x00, 0x00, 0xff, 0x80));
		g.drawLine((int)Self.target.lastRelativeLocation.x+90, (int)Self.target.lastRelativeLocation.y+90, 90, 90);
		
		g.setColor(new Color(0xff, 0x00, 0x00, 0x80));//red
		g.drawLine((int)Self.target.relativeLocation.x+90, (int)Self.target.relativeLocation.y+90, 90, 90);
		
		g.setColor(new Color(0x00, 0xff, 0x00, 0x80));//green
		g.drawLine((int)Self.target.predictedNextRelLoc.x+90, (int)Self.target.predictedNextRelLoc.y+90, 90, 90);
		
		g.setColor(new Color(0xff, 0xff, 0x10, 0x80 ));//yellow
		g.drawLine((int)Self.firingAt.x, (int)Self.firingAt.y, (int)Self.location.x, (int)Self.location.y);
		
		g.setColor(new Color(0xff, 0x00, 0xff, 0x70));
		g.drawLine((int)Self.movingTo.x, (int)Self.movingTo.y, (int)Self.location.x, (int)Self.location.y);
	}

	public void onSkippedTurn(SkippedTurnEvent e)
	{
		System.out.println("\n+-+-+-=====!skipped a turn!=====+-+-+-\n");
	}
	
	public void onBulletHit(BulletHitEvent e)
	{
		Self.lastTimeHitTarget = Self.time;
	}
	
	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) 
	{
		//*
		if(Self.target == null) 
		{
			Self.target = new Other(e);
		}
		else 
		{
			if(Self.haveTgt)Self.target.update(e);//haveTgt is set to false on the first update and set to true at the end of run()
		}
		//*/

	}
	
	public void onStatus(StatusEvent e)
	{
		Self.updateSelf(e.getStatus());//this is because i cannot access the run() function above
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) 
	{
		// Replace the next line with any behavior you would like
		Self.moving = false;
		//Self.reverse *= -1;
	}
	
	public void onHitRobot(HitRobotEvent e)
	{
		Self.reverse *= -1;
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) 
	{
		// Replace the next line with any behavior you would like
		Self.moving = false;
		Self.reverse *= -1;
	}	
	
	public void onBattleEnded(BattleEndedEvent e)
	{
		Self.target = null;
		
		Self.time = 0;
	}
	
	public void onRoundEnded(RoundEndedEvent e)
	{
		Self.target = null;
		
		//Self.time = 0;
	}
	
	
	
}
