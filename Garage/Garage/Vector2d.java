package Garage;
//import robocode.*;

//import java.util.*;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
public class Vector2d extends Vector
{
	public double x;
	public double y;
	public double mag;
	public boolean magCurrent;
	public static double root2Over2 = Math.sqrt(2)/2;
	public static final Vector2d up = new Vector2d(0,1);
	public static final Vector2d right = new Vector2d(1,0);
	public static final Vector2d down = new Vector2d(0,-1);
	public static final Vector2d left = new Vector2d(-1,0);
	public static final double radToDeg = 180.0/Math.PI;
	public static final double degToRad = Math.PI/180.0;
	
	
	
	//constructors
	Vector2d()
	{
		length = 2;
		x = 0.0;
		y = 0.0;
		mag = 0.0;
		magCurrent = false;
	}
	
	Vector2d(double xi, double yi)
	{
		length = 2;
		x = xi;
		y = yi;
		magCurrent = false;
	}
	
	Vector2d(Vector2d v)
	{
		length = 2;
		x = v.x;
		y = v.y;
		mag = v.mag;
		magCurrent = v.magCurrent;
	}
	
	//operations
	public Vector2d plus(Vector2d v)
	{
		return new Vector2d(v.x + x, v.y + y);
	}
	
	public Vector2d minus(Vector2d v)
	{
		return new Vector2d(x - v.x, y - v.y);
	}
	
	public double dot(Vector2d v)
	{
		return x*v.x + y*v.y;
	}
	
	public Vector2d getNormalized()	
	{
		if(!magCurrent)updateMag();
		
		return (mag != 0.0)? new Vector2d(x/mag, y/mag) : this;
	}
	
	public static Vector2d degToVector(double deg)
	{
		//sort negative signs for the 0 degree being up instead of right, and 90 being right
		return new Vector2d(Math.sin(deg*degToRad),Math.cos(deg*degToRad));
		
	}//end of degToVector
		
	public double[] getArray()
	{
		double[] ret = new double[length];
		ret[0] = x;
		ret[1] = y;
		return ret;
	}
	
	public double getDirDegrees()
	{
		if(magCurrent && mag == 1)
		{
			return (x >= 0.0)? 	(	(y >= 0.0)? radToDeg*Math.acos(y) : 90+ radToDeg*Math.acos(x)	):
								(	(y <= 0.0)? 180+ radToDeg*Math.acos(-y) : 270+ radToDeg*Math.acos(-x)	);
		}
		else
		{
			updateMag();
			double mult = 1.0/mag;
			return (x >= 0.0)? 	(   (y >= 0.0)? radToDeg*Math.acos(y*mult) : 90+ radToDeg*Math.acos(x*mult)    ):
								(   (y <= 0.0)? 180+ radToDeg*Math.acos(-y*mult) : 270+ radToDeg*Math.acos(-x*mult)   );
		}
	}
	
	public double getMag()
	{
		return Math.sqrt(x*x + y*y);
	}
	
	public Vector2d getLeft()
	{
		return new Vector2d(y,-x);
	}
	
	public Vector2d getRight()
	{
		return new Vector2d(-y,x);
	}
	
	public Vector2d getBack()
	{
		return new Vector2d(-x, -y);
	}
	
	public void updateMag()
	{
		mag = Math.sqrt(x*x + y*y);
		magCurrent = true;
	}
	
	public void normalizeMe()
	{
		if(!magCurrent) updateMag();
		if(mag != 0.0)
		{
			x /= mag;
			y /= mag;
			mag = 1;
		}
	}

	
	
	/*//ready for testing. START HERE////////////////////////////////////////////////
	public static void main(String[] args)
	{
		Vector2d[] vecs = new Vector2d[720];
		for(int i=0; i<720; i++)
		{
			System.out.println("i= " +i+ ": "+ Vector2d.degToVector(((double)i)/2).getDirDegrees());
		}
	}
	
	//*/
		
		
		
}//end of class