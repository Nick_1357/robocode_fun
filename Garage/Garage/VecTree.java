package Garage;
//import robocode.*;

//import java.awt.Color;
//import java.util.ArrayList;
//import java.util.Scanner;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

public class VecTree
{
	public Node root;
	public Node trav;
	public int count;
	public int height;
	public boolean needBallance;
	public boolean inListOrder;
	
	VecTree()
	{
		root = null;
		trav = null;
		count = 0;
		height = 0;
		needBallance = false;
		inListOrder =false;
	}
	
	VecTree(double dat, int mapping)
	{
		root = new Node(dat, mapping);
		trav = null;
		count = 1;
		height = 1;
		needBallance = false;
		inListOrder =false;
	}
	
	//do not delete: ever
	VecTree(Node n)//copies the node given, and makes a new node for itself
	{
		root = new Node(n);
		trav = null;
		count = 1;
		height = 1;
		needBallance = false;
		inListOrder =false;
	}
	
	VecTree(VecTree t)//this is a deep copy and is expensive
	{
		root = new Node(t.root, this);
		trav = null;
		count = 0;
		height = 1;
		needBallance = t.needBallance;
		inListOrder = t.inListOrder;
	}
	
	public Node add(double dat, int mapping)//important: returns the node placed in the tree
	{
		if(root==null){}
		else if(root.index == 0)root = null;
		
		return add(new Node(dat, mapping));
	}
	
	public Node add(Node n)//important: returns the node placed in the tree
	{
		if(n==null)return null;
		
		if(root == null)
		{
			root = n;
			count = 1;
			height = 1;
			needBallance = false;
			
			n.depth = 0;
			n.parent = null;
			n.lesser = null;
			n.greater = null;
			
			return n;
		}
		
		int i=1;
		trav = root;
		Node ret = null;
		//nodes of the same index are simply updated instead of replacing entirely
		while(trav != null)
		{	
			//is the new one lesser?
			if(n.index < trav.index)
			{
				//System.out.print("lesser "+ n.index+", ");/////////////////////////////debug
				if(trav.lesser==null)
				{
					n.depth = i;
					n.parent = trav;
					trav.lesser = n;
					ret = n;
					
					break;
				}
				else trav=trav.lesser;
			}
			else 
			{	//is it equal?
				if(n.index == trav.index)
				{
					trav.dat=n.dat;
					Node tmpPtr = trav;
					trav = null;
					return tmpPtr;//this is when it is different from all the rest
				}
				else //it must be greater
				{
					//System.out.print("greater "+ n.index+", ");////////////////////////debug
					if(trav.greater==null)
					{
						n.depth = i;
						n.parent = trav;
						trav.greater = n;
						ret = n;
						
						break;
					}
					else
					{
						trav = trav.greater; 
					}
				}
			}
			
			i++;
		}
		
		trav = null;
		count ++;
		if( (i>2)?( (/**/((float)(1<<i))/count/**/) >1.15625 ): false )
		{
			needBallance = true;
		}
		
		if(ret.depth >= height)height = ret.depth+1;
		return ret;
	}
	
	public void ballanceTree()//works on any tree formation and **special** works after getListOrder() has been called externally
	{
		if(count<4)return;
		
		int hold = count;
		int newHeight = 0;
		while(hold !=0)
		{
			hold = hold>>1;
			newHeight++;
		}
		
		height = newHeight;
		int size = (1<<newHeight)-1;
		int blanks = size - count;
	
		//get tree into lesser->greater list order
		Node[] nodes = getListOrder();
		
		Node[] withDummyNodes = new Node[size+1];																			//$$$$
		for(int i=0, offset = 0; i<size; i++)
		{
			if((i&1) == 0 && (i>>1) < blanks)//if (i is even and small enough to be a blank)
			{
				withDummyNodes[i] = null;
				offset++;
			}
			else
			{
				withDummyNodes[i] = nodes[i-offset];
			}
		}
		withDummyNodes[size]= null;//gives the root a null to point to in the loops below
			
		int entriesInRow = 0;//*&^^
		int separation = 1;//@##%
		int up = 0;	//***!
		int down = 0;
		
		for(int i=0; i< newHeight; i++)
		{
			entriesInRow = (1<<(newHeight-i-1));	//*&^^
			down = up;
			up = separation;	 //***! //@##%
			separation*=2;
			
			for(int j=0; j< entriesInRow; j++)
			{
				//treat the tree like it has (j,i)on a -y system of Cartesian coordinates (like the screen's coordinates)
				hold = ( (1<<i)-1 ) + j*separation;//@##%                                 //***!!!!warning: REUSING HOLD FROM ABOVE!!!!***
				if(withDummyNodes[hold]!= null) 
				{
					withDummyNodes[hold].lesser = (i==0)? null:withDummyNodes[hold-down];//$$$$
					withDummyNodes[hold].greater = (i==0)? null:withDummyNodes[hold+down];//$$$$
					withDummyNodes[hold].depth = newHeight-(i+1);
				}
				
				if(withDummyNodes[hold]!= null)
				{
					withDummyNodes[hold].parent = ((j&1)==0)? withDummyNodes[hold+up]: withDummyNodes[hold-up];//***!
				}
			}
		}
		root = withDummyNodes[(1<<(newHeight-1))-1 ];
		needBallance = false;
		inListOrder = false;
	}

	public Node[] getListOrder()//warning: destroys tree structure; note: Node[0] is least valuable index
	{	
		if(count == 0) return null;
		if(count == 1) 
		{
			Node[] ret = new Node[1] ;
			ret[0]= root;
			return ret;
		}

		if(inListOrder)
		{
			Node[] ret = new Node[count];
			trav = root;
			
			while (trav.lesser != null)
			{
				trav = trav.lesser;
			}
			//now trav is at the least value in the sorted list
			
			for(int i=0; i<count; i++)
			{
				ret[i] = trav;
				trav = trav.greater;
			}
			
			trav = null;
			return ret;
		}
		
		needBallance = true;
		inListOrder = true;
	
		trav = root;

		Node last = new Node();//holds the most recently organized node.
		
		for(int i=0; i< count; )
		{
			//down/left movement
			while(trav.lesser != null && trav.lesser != last)
			{
				trav = trav.lesser;
			}
			
			trav.lesser = last;
			last.greater = trav;//this is why last is a dummy node and not null
			
			last = trav;
			i++;
			
			if(i == count)break;
			
			if(trav.greater == null)
			{
				while(trav.index <= last.index)
				{
					//travel in descending order to the last un explored fork
					while(trav.lesser == trav.parent || trav.index <= last.index)
					{
						trav = trav.parent;
					}
					//if(trav==root)break;// this might have broken older fixes
					
					//up/right/down movement
					last.greater = trav;//greater connection
					trav.lesser = last;//lesser connection
					last = trav;//last sort added was the parent of the fork
					
					i++;
					if(i == count)break;
					
					trav = (trav.greater!=null)? trav.greater: (trav.parent!=null)? trav.parent: root;
				}
			}//down/right movement
			else trav = trav.greater;
		}
		
		Node[] ret = new Node[count];
		
		for(int j=ret.length-1; j>=0; j--)
		{
			ret[j] = last;
			last = last.lesser;
		}
		
		//snip off the dummy node for the garbage collector
		ret[0].lesser.greater = null;
		ret[0].lesser = null;
		
		return ret;
		
	}
	
	public Node getNodeByIndex(int index)
	{
		Node n = root;
		Node uuu;
		trav = n;
		while(trav != null)
		{
			if(trav.index == index)
			{
				uuu = trav;
				trav = null;//every function that uses trav, leaves it null when it's finished
				return uuu;
			}
			
			trav = (index < trav.index)? trav.lesser : trav.greater;

		}
		//if it makes it out here, it's not in the tree
		//values not in the tree are effectively zero and the default node value is zero
		return new Node();
	}
	
	public String toString()
	{
		return "count:"+count+ toString(root);
	}
	private String toString(Node n)
	{
		String s = " "+n.toString();
		
		if(n.lesser != null) s += toString(n.lesser);
		if(n.greater != null)s += toString(n.greater);
		
		return s;
	}
	
	
	
	//
	//
	//
	//
	//
	/*crash test dummy driver
	// 
	// 
	public static void main(String[] args)
	{while(true){
		String loopControl;
		
		int testSize = 1000;
		VecTree test = new VecTree();
		//--------------------------------test 1
		int[] rando = new int[testSize];
		Node[] nodes = new Node[testSize];
		for(int i=0; i<testSize;i++)
		{
			rando[i]=i;
		}
		for(int i=0, tmp; i<testSize; i++)
		{
			tmp = ((int)(Math.random()*100)) % testSize;
			nodes[i] = new Node(0.5, i);
			
			rando[i] = rando[i]^rando[tmp];
			rando[tmp] = rando[tmp]^rando[i];
			rando[i] = rando[i]^rando[tmp];
		}
		//int rand[] = {5,0,6,3,1,2,4};//injection point to test specific sequences
		
		for(int i=0; i< testSize; i++)
		{
			System.out.print(rando[i]+ ", ");///////////////////////////////
		}
		//System.out.println();///////////////////////////
		
		for(int i=0; i<testSize; i++)
		{
			test.add(nodes[rando[i]]);
		}
		//
		
		//-----------------------------------------test2
		for(int i=0; i< testSize; i++)
		{
			
			//test.add(Math.random()+i, i);//worst case scenario is for entries to be entered in sequential order.
		}
		//
		
		//-----------------------------------------test 3
		for(int i=testSize-1; i>=0; i--)
		{
			
			//test.add(Math.random()+i, i);//worst case scenario is for entries to be entered in sequential order.
		}
		//
		
		System.out.println(test.toString());
		
		test.ballanceTree();
		System.out.println("\n------------------------------------------\n");
		
		
		System.out.println(test.toString());
		System.out.print("enter something to continue:");
		
		loopControl = (new Scanner(System.in)).next();
		System.out.println("\n");
		
	 }//end of while(true)	
	}//end of main() for testing
	//*/ 
	
	
	
	
}//end of tree
