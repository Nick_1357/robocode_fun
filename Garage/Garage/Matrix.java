package Garage;

public class Matrix 
{
	public int m;// an mxn matrix
	public int n;
	public boolean detNEG;//odd permutations cause the determinant to change signs
	public int[] swaps;
	
	double[][] mat;
	
	Matrix()
	{}//empty default contructor
	
	Matrix(double[] vec, boolean t)
	{
		if(t)
		{
			m = 1;
			n = vec.length;
			
			swaps = new int[m];
			mat = new double[m][n];
			
			for(int i=0; i<m; i++)
			{
				mat[0][i] = vec[i];
				swaps[i] = i;
			}
		}
		else
		{
			m = vec.length;
			n = 1;
			
			swaps = new int[m];
			mat = new double[m][n];
			
			for(int i=0; i<m; i++)
			{
				mat[i][0] = vec[i];
				swaps[i] = i;
			}
		}
	}
	
	Matrix(double[][] vecs, boolean t )
	{

		m = (t)?vecs[0].length:vecs.length;
		n = (t)?vecs.length:vecs[0].length;
		
		swaps = new int[m];
		mat = new double[m][n];
		
		for(int i=0; i<m; i++)
		{
			for(int j=0; j<n; j++)
			{
				mat[i][j] = vecs[(t)?j:i][(t)?i:j];	
				//	matrix made of 3 {x, y, z} vectors: {0,1,2} , {3,3,3}, {7,6,5}
				//
				//						x 0 3 7
				//						y 1 3 6
				//						z 2 3 5
				//
				//		vecs[0][0] = 0 : vecs[0][1] = 1 : vecs[0][2] = 2
				//		vecs[1][0] = 3 : vecs[1][1] = 3 : vecs[1][2] = 3
				//		vecs[2][0] = 7 : vecs[2][1] = 6 : vecs[2][2] = 5 
				//
			}
			swaps[i]= i;
		}
	}
	
	Matrix(Matrix copee, boolean transposeThisMatrix)
	{
		if(transposeThisMatrix)
		{
			m = copee.n;
			n = copee.m;
			
			mat = new double[m][n];
			swaps = new int[m];
			
			for(int i=0; i<m; i++)
			{
				for(int j=0; j<n; j++)
				{
					mat[i][j] = copee.mat[j][copee.swaps[i]];
				}
				swaps[i] = i;
			}
		}
		else
		{
			m = copee.m;
			n = copee.n;
			
			mat = new double[m][n];
			swaps = new int[m];
			
			for(int i=0; i<m; i++)
			{
				for(int j=0; j<n; j++)
				{
					mat[i][j] = copee.mat[copee.swaps[i]][j];
				}
				swaps[i] = i;
			}
		}
	}
	
	//manipulating the matrix//////////////////////////////////////////////////
	private void rowMultiplication(double multiple, int rowChanging)
	{
		for(int i=0; i < n; i++)
		{
			if(mat[rowChanging][i] == 0)continue;
			
			mat[rowChanging][i] *= multiple;
		}
	}
	
	private void rowAddition(double multiple, int rowNotChanging, int rowChanging)
	{
		for(int i=0; i<n; i++)
		{
			mat[rowChanging][i] += multiple*mat[rowNotChanging][i];
		}
	}
	
	private void getInUpperTri()
	{
		
		boolean trap = false;//>>>><<<<
		
		for(int i=0, j=0, look; i<m && i+j < n; i++)//j keeps track of free variables and/or columns of zeroes 
		{
			look = i;
			if(mat[swaps[look]][i+j]==0)
			{
				while(mat[swaps[look]][i+j]==0)
				{
					look++;//moving down the column looking for a non-zero 
					if(look == m)
					{
						trap = true;//>>>><<<<
						j++;//need to check next column
						break;
					}
				}
				if(trap)//if trap is true, it's in a row of zeroes
				{
					trap = false;//>>>><<<<
					i--;//need to check the same row
					continue;
				}
		
				swaps[i] ^= swaps[look]; //row swap
				swaps[look] ^= swaps[i];
				swaps[i] ^= swaps[look];
				for(int dbug = 0; dbug<m; dbug++)
				{
					System.out.println("swaps["+dbug+"] = " + swaps[dbug]);
				}
				System.out.println(this+"DEBUG\tswapped\n");
			}
			
			//making it here means mat[look][i+j] is non-zero
			rowMultiplication(1.0/mat[swaps[i]][i+j], swaps[i]);//set pivot to 1

			//reduce rows below
			for(int k=1; i+k<m; k++)
			{
				rowAddition(-mat[swaps[i+k]][i+j], swaps[i], swaps[i+k]);
			}
			
			
			for(int dbug = 0; dbug<m; dbug++)
			{
				System.out.println("swaps["+dbug+"] = " + swaps[dbug]);
			}
			System.out.println(this+"DEBUG\n");
			
			
		}//m loop
		
	}
	
	private Matrix getInReducedRowEch(boolean inUpperTri, boolean inverting)//returns either null or the inverse depending on the inverting boolean
	{
		Matrix I = null;//created here for scope below
		if(inverting)
		{
			if(m!=n)return null;
			double[][] ident = new double[m][m];
			
			for(int i=0; i<m; i++)
			{
				for(int j=0; j<m; j++)
				{
					ident[i][j] = (i==j)? 1:0;
				}
			}
			
			 I = new Matrix(ident, false);
		}
		if(!inUpperTri)
		{
			boolean trap = false;//>>>><<<<1of3
			
			for(int i=0, j=0, look =0; i<m && i+j < n; i++)//j keeps track of free variables and/or columns of zeroes 
			{
				look = i;
				if(mat[swaps[look]][i+j]==0)
				{
					while(mat[swaps[look]][i+j]==0)
					{
						look++;//moving down the column looking for a non-zero 
						if(look == m)
						{
							trap = true;//>>>><<<<2of3
							j++;//need to check next column
							break;
						}
					}
					if(trap)//if trap is true, it's in a column of zeroes
					{
						trap = false;//>>>><<<<3of3
						i--;//need to check the same row
						continue;
					}
			
					swaps[i] ^= swaps[look]; //row swap
					swaps[look] ^= swaps[i];
					swaps[i] ^= swaps[look];
					/*
					for(int dbug = 0; dbug<m; dbug++)
					{
						System.out.println("swaps["+dbug+"] = " + swaps[dbug]);
					}
					System.out.println(this+"DEBUG\tswapped\n");
					*/
				}//end of if(mat...)
				
				//making it here means mat[look][i+j] is non-zero
				
				if(inverting)I.rowMultiplication(1.0/mat[swaps[i]][i+j], swaps[i]);//update inverse matrix before the information is lost
				rowMultiplication(1.0/mat[swaps[i]][i+j], swaps[i]);//set pivot to 1
					
				//reduce rows below
				for(int k=1; i+k<m; k++)
				{
					if(inverting)I.rowAddition(-mat[swaps[i+k]][i+j], swaps[i], swaps[i+k]);//update inverse matrix before the information is lost
					rowAddition(-mat[swaps[i+k]][i+j], swaps[i], swaps[i+k]);
				}
				
			}//m loop
		}
		
		/////////////////////////from here, it is assumed the matrix is in U-form of the LDU decomposition //////////////////////////////////////////////////
		
		//reduce rows above
		for(int i= m-1, j; i>=0; i--)//start at the bottom row
		{
			j=0;
			while(j<n&&mat[swaps[i]][j]==0)j++;//find the pivot
			if(j==n)continue;//row of zeroes
			
			for(int k = i-1; k>=0; k--)
			{
				if(inverting)I.rowAddition(-mat[swaps[k]][j], swaps[i], swaps[k]);//update inverse matrix before the information is lost
				rowAddition(-mat[swaps[k]][j], swaps[i], swaps[k]);//zero the entries above
			}
			
		}
		
		return I;
	}

	
	//math on the matrix///////////////////////////////////////////////////////
	public Matrix times(Matrix leftOfMe)
	{
		double[][] newMat = new double[n][leftOfMe.m];
		
		for(int i=0; i<leftOfMe.m; i++)//new matrix rows
		{
			for(int j=0, sum; j<n; j++)//new matrix columns
			{
				sum = 0;
				for(int k=0; k<m; k++)//summing products
				{
					sum += mat[swaps[k]][j]*leftOfMe.mat[leftOfMe.swaps[i]][k];
				}
				newMat[j][i] = sum;
			}
		}
		
		return new Matrix(newMat, false);
	}
	
	public Matrix getInverse()
	{
		Matrix ret = null;
		if(m!=n)//is it square?
		{
			//transpose and multiply, must be looking for the left-inverse
			ret = times(new Matrix(mat, true));
		}
		else
		{
			ret = new Matrix(this, false);
		}
		
		return ret.getInReducedRowEch(false, true);
	}
	
	public double getDet()
	{
		if(m!=n)return 0.0;
		getInUpperTri();
		
		double sum = 1.0;
		for(int i=0; i<m; i++)
		{
			sum *= mat[i][i];
		}
		
		return sum;
	}
	
	public Matrix existInSpan(double[] b)// Ax = b; find x, given b ----- returns null for invalid computations
	{
		if(mat == null || mat.length<1)return null;
		if(b.length != mat[0].length)return null;
		
		return new Matrix(b, false).times(getInverse());
	}
	
	public String toString()
	{
		String ret = "";
		for(int i=0; i<mat.length; i++)
		{
			ret+="|";
			for(int j=0; j<mat[0].length; j++, ret+= "|")
			{
				ret +=  mat[swaps[i]][j];
			}
			ret+="\n";
		}
		return ret;
	}
	
	
	
	
	/////////////////////////////////////////////////test driver/////////////////////////////
	/*
	public static void main(String[] Args)
	{
		double[][] vecs = {{-7,-9,-1}, {8,3,-6}, {5,-5,-2}, {1,1,1}};//
		double[][] vecs2 = {{4,2}, {7,6}};
		
		Matrix myMat = new Matrix(vecs, false);
		Matrix myOtherMat = new Matrix(vecs2, false);//new Matrix(this, true)
		
		//System.out.println(myMat);
		System.out.println("\n"+ myOtherMat);
		
		Matrix newMat = myOtherMat.getInverse();
		
		//myMat.getInReducedRowEch(false);
		
		System.out.println(newMat);
		
		myMat.getInReducedRowEch(false, false);
		
		System.out.println(myMat);
		
	}
	
	//*/
	
}
