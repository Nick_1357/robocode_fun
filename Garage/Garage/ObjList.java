package Garage;
import java.util.*;
import robocode.*;

/**
 * internal MyNode class of the List
 */
class MyNode<T>
{
	public T data;
	public MyNode<T> next;
	
	public MyNode(T d)
	{ //initialize the attributes
		data = d;
		next = null;
	}
	
	public MyNode(T d, MyNode n)
	{
		data = d;
		next = n;
	}
	
}

////////////////////////////////////////////////////////////////start of class ObjList
public class ObjList<T> 
{ 
	//attributes
	public MyNode<T> head;
	public MyNode<T> tail;
	
	public int size;

	////constructors
	public ObjList()
	{ //default constructor
		head = null;
		tail = null;
		size = 0;
	}
	
	//put in back of list
	public MyNode<T> addLast(T data)
	{
		MyNode<T> newNode = new MyNode<T>(data);
		size++;
		
		//empty list, create it as the first MyNode
		if(head == null)
		{
			tail = newNode;
			head = newNode;
		}
		else//populated list
		{
			tail.next = newNode;
			tail = newNode;
		}
		return tail;
	}

	//put in front of list
	public MyNode<T> addFirst(T data)
	{
		MyNode<T> newNode = new MyNode<T>(data);
		size++;
		
		//empty list
		if(head == null)
		{
			tail = newNode;
			head = newNode;
		}
		else//populated list
		{
			newNode.next = head;
			head = newNode;
		}
		return head;
	}

	
	public void removeFirst()
	{
		//empty list
		if(this.head == null)
		{
			return;
		}
		//one element in list
		if(this.head == this.tail)
		{
			this.head = null;
			this.tail = null;
		}
		else//populated list
		{
			MyNode<T> temp = head;
			head = temp.next;
			temp.next = null;
			temp = null;
		}
		
		this.size--;
	}
	
	public void removeLast()
	{
		//empty list
		if(head == null)
		{
			return;
		}
		
		//one element in list
		if(head == tail)
		{
			head = null;
			tail = null;
		}
		else//populated list
		{
			MyNode<T> temp = head;
			MyNode<T> temp2 = temp.next;
			while(temp2 != tail)
			{
				temp = temp2;
				temp2 = temp.next;
			}
			tail = temp;
			tail.next = null;
		}
		this.size--;
	}
	

	public String toString()
	{
		MyNode<T> temp = this.head;
		String output = "";
		while(temp != null)
		{
		  output += temp.data + " " ;
		  temp = temp.next;
		}
		return output;
	}
	

	
	//=============================================================
	//==== A DRIVER to test the ObjList class - REMOVE WHEN DONE ====
	//=============================================================
	
	
	
			/*<--add second / to use driver
	
	
	

	public static void main(String args[])
	{

	   
	   
	}
	//==== END of the DRIVER =============
	//*/	
}


