package Garage;
import java.util.*;

import robocode.*;

import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
	
/**
 * WeAllAreRobertPaulson - a robot by (cIaoaI)
 */
public class Vector3d extends Vector
{
	public double x;
	public double y;
	public double z;
	public double mag;
	public boolean magCurrent;
	
	//constructors
	Vector3d()
	{
		length = 3;
		x = 0.0;
		y = 0.0;
		z = 0.0;
		magCurrent = false;
	}
	
	Vector3d(double xi, double yi, double zi)
	{
		length = 3;
		x = xi;
		y = yi;
		z = zi;
		magCurrent = false;
	}
	
	Vector3d(Vector3d v)
	{
		length = 3;
		x = v.x;
		y = v.y;
		z = v.z;
		mag = v.mag;
		magCurrent = v.magCurrent;
	}
	
	
	//operations
	public Vector3d plus(Vector3d v)
	{
		return new Vector3d(v.x+x, v.y+y, v.z+z);
	}
	
	public Vector3d minus(Vector3d v)
	{
		return new Vector3d(x - v.x, y - v.y, z - v.z);
	}
	
	public double dot(Vector3d v)
	{
		return x*v.x + y*v.y + z*v.z;
	}
	
	public Vector3d getNormalized()
	{
		if(!magCurrent)updateMag();
		
		return (mag != 0.0)? new Vector3d(x/mag, y/mag, z/mag) : this;
	}
	
	public double[] getArray()
	{
		double[] ret = new double[length];
		ret[0] = x;
		ret[1] = y;
		ret[2] = z;
		return ret;
	}
	
	public void updateMag()
	{
		mag = Math.sqrt(x*x + y*y + z*z);
		magCurrent = true;
	}
	
	public void normalizeMe()
	{
		if(!magCurrent)updateMag();

		if(mag != 0.0)
		{
			x /= mag;
			y /= mag;
			z /= mag;
			mag = 1;
		}
	}
}
