package Garage;
import java.util.*;
import robocode.*;

public class FireEvent 
{
	public long timeFired;
	public int timeToTgt;
	public long timeArriving;
	public Vector2d startLocation;
	public Vector2d direction;
	public Vector2d targetLocation;
	public Situation now;
	public Bullet proj;
	
	public static ObjList<FireEvent> myHistory = new ObjList<FireEvent>();
	
	
	public FireEvent()
	{}//empty default
	
	public FireEvent(long t, Vector2d start, Vector2d tgt, Bullet b)
	{
		timeFired = t;
		timeToTgt = (int)( Self.firingAt.mag/b.getVelocity() );
		timeArriving = timeFired + timeToTgt;
		startLocation = start;
		targetLocation = tgt;
		proj = b;

		now = new Situation();
		
		direction = tgt.minus(start).getNormalized();
	}

	
}
