package Garage;
//import java.util.*;

import robocode.*;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
	
public class Other
{	////////////////////////////////// variables given directly by onStatus(event e)
	public String name;				//
	public double bearingD;			//
	public double distance;			//
	public double energy;			//
	public double headingD;			//
	public double velocity;			//
	//////////////////////////////////
	
	public double rotation;
	public double acceleration;
	
	public Vector2d relativeLocation;
	public Vector2d lastRelativeLocation;
	public Vector2d predictedNextRelLoc;
	
	public Vector2d location;
	public Vector2d lastLocation;
	public Vector2d predictedNextLoc;
	
	public Vector2d direction;
	
	public long timeLastScanned;
	
	
	//public double currentGuessD;
	
	Other()
	{}//empty default
	
	Other(ScannedRobotEvent e)
	{
		timeLastScanned = Self.time;
		name = e.getName();
		bearingD = e.getBearing();
		distance = e.getDistance();
		energy = e.getEnergy();
		headingD = e.getHeading();
		velocity = e.getVelocity();
		
		direction = Vector2d.degToVector(headingD);
		relativeLocation = Vector2d.degToVector(Self.headingD + bearingD);
		relativeLocation.x *= distance;
		relativeLocation.y *= distance;
		relativeLocation.updateMag();
		
		lastRelativeLocation = relativeLocation;
		location = Self.location.plus(relativeLocation);
		location.updateMag();
		lastLocation = location;
		
		predictedNextLoc = location;
		predictedNextLoc.x += direction.x*velocity;
		predictedNextLoc.y += direction.y*velocity;
		predictedNextLoc.updateMag();
		
		predictedNextRelLoc = predictedNextLoc.minus(Self.futureLocation);
		predictedNextRelLoc.updateMag();
	}
	
	public void update(ScannedRobotEvent e)//update from a scan (new info)
	{
		Self.haveTgt = false;
		
		rotation = headingD;
		acceleration = velocity;
		
		timeLastScanned = Self.time;
		bearingD = e.getBearing();
		distance = e.getDistance();
		energy = e.getEnergy();
		
		headingD = e.getHeading();
		velocity = e.getVelocity();
		
		direction = Vector2d.degToVector(headingD);
		lastLocation = location;
		lastRelativeLocation = relativeLocation;
		
		relativeLocation = Vector2d.degToVector(Self.headingD + bearingD);
		relativeLocation.x *= distance;
		relativeLocation.y *= distance;
		relativeLocation.updateMag();
		
		location = Self.location.plus(relativeLocation);
		location.updateMag();
		
		predictedNextLoc = location;
		predictedNextLoc.x += direction.x*(velocity);
		predictedNextLoc.y += direction.y*(velocity);
		predictedNextLoc.updateMag();
		
		predictedNextRelLoc = predictedNextLoc.minus(Self.futureLocation);
		predictedNextRelLoc.updateMag();
		
		
		double tmp = Self.firingAt.minus(Self.location).getMag();
		Self.bulletPower = Math.min(3 * relativeLocation.getNormalized().dot(Self.firingAt.minus(Self.location).getNormalized()) * 180/tmp, 3);//based on confidence the shot will hit
		Self.gunCorrection = tmp/(20 -3*Self.bulletPower) ;//number of frames until the bullet arrives at the firingAt location
		
		
		//setting this will be AI intensive, but for now, just lead them a proportional amount.
		BehaviorPrediction.guess();
		BehaviorPrediction.updateDecisionMap();
		
		rotation = (rotation - headingD > 10)? (360-rotation+headingD) : ((rotation - headingD < -10)? (360-headingD + rotation): (rotation - headingD));
		acceleration -= velocity;
		
		System.out.println("realAccel: " + acceleration + "realRotat: " + rotation);
		
		
		/*
		Self.firingAt.x = location.x+ (Self.gunCorrection*velocity*direction.x);
		Self.firingAt.y = location.y+ (Self.gunCorrection*velocity*direction.y);
		
		if(Self.firingAt.x > Self.arenaWidth-18)Self.firingAt.x = Self.arenaWidth-18;
		else if(Self.firingAt.x <18) Self.firingAt.x = 18;
		if(Self.firingAt.y > Self.arenaHeight-18)Self.firingAt.y = Self.arenaHeight-18;
		else if(Self.firingAt.y <18) Self.firingAt.y = 18;
		
		Self.firingAt.updateMag();*/
		
	}
	
	//public static Vector2d predictFireDirection(double v, double h, double b)
	//{
		//using
		//Self.velocity
		//Self.heading
		//Self.target.bearing (Other.bearing)
		//to answer how much he leads my position while aiming
		
	//}
	

}

