package Garage;

public class BehaviorPrediction //currently bogus. will try again later with filtering and noise reduction ideas.
{
	private static Situation[] situations= new Situation[11];
	private static double[][] theMat = new double[20][11];
	private static int mostRecent = 0;
	private static int count = 0;
	private static double[] teachRotation = new double[11];
	private static double[] teachAcceleration = new double[11];
	private static double[] thetasRotation = new double[11];
	private static double[] thetasAcceleration = new double[11];
	
	public BehaviorPrediction()
	{
		//fill theMat with zeroes
		for(int i=0; i<theMat.length; i++)
		{
			for(int j=0; j<theMat[0].length; j++)
			{
				theMat[i][j] = 0;
			}
		}
	}
	
	//map conditions of the battle field to the bot's decisions for acceleration and rotation
	
	public static void updateDecisionMap()
	{
		
		situations[mostRecent] = new Situation();
		
		teachRotation[mostRecent] = Self.target.rotation;
		teachAcceleration[mostRecent] = Self.target.acceleration;
		
		theMat[mostRecent][0]=situations[mostRecent].timeSinceLastFire;
		theMat[mostRecent][1]=situations[mostRecent].timeSinceLastHit;
		theMat[mostRecent][2]=situations[mostRecent].relativeLocation.mag;
		theMat[mostRecent][3]=situations[mostRecent].relativeLocationNextTurn.mag;
		theMat[mostRecent][4]=situations[mostRecent].myVelocity;
		theMat[mostRecent][5]=situations[mostRecent].myEnergy;
		theMat[mostRecent][6]=situations[mostRecent].myHeading;
		theMat[mostRecent][7]=situations[mostRecent].theirVelocity;
		theMat[mostRecent][8]=situations[mostRecent].theirEnergy;
		theMat[mostRecent][9]=situations[mostRecent].theirHeading;
		theMat[mostRecent][10]=situations[mostRecent].theirBearing;
		
		if(mostRecent == 10)
		{
			updateThetas();
			mostRecent=0;
		}
		else mostRecent++;
		if(count<11)count++;
	}
	
	private static void updateThetas()
	{
		Matrix mat = new Matrix(teachRotation, false);
		Matrix mat1 = new Matrix(theMat, false);
		mat = mat.times(mat1.getInverse());
		
		//cheating by only using square matrices
		// [theta] = [([X']*[X])^-1]*[X']*[Y]      where [X] is the data matrix, [X'] is [X]-transpose and [Y] is the answer vector 
		// ([X']*[X])^-1 = ([X]^-1)*([X']^-1)
		// so [theta] = ([X]^-1)*[Y] when [X] is square
		
		
		//System.out.println("rotation:");
		for(int i=0; i<11; i++)
		{
			thetasRotation[i] += mat.mat[0][i];
			thetasRotation[i] *= 0.5;
			//System.out.print( (int)thetasRotation[i] + "|");
		}
		//System.out.println();
		
		
		///////////////////////////
		//now same for acceleration 
		mat = new Matrix(teachAcceleration, false);
		mat1 = new Matrix(theMat, false);
		
		mat = mat.times(mat1.getInverse());
		
		//System.out.println("accel:");
		for(int i=0; i<11; i++)
		{
			thetasAcceleration[i] += mat.mat[0][i];
			thetasAcceleration[i] *= 0.5;
			//System.out.print( (int)thetasAcceleration[i] + "|");
		}
		//System.out.println();
	}
	
	public static void guess()
	{
		double accGuess = 0;
		double rotGuess = 0;
		
		for(int i=0; i< 11; i++)
		{
			accGuess += thetasAcceleration[i] * theMat[mostRecent][i];
			rotGuess += thetasRotation[i] * theMat[mostRecent][i];
		}
		
		System.out.println("AccGuess: " + accGuess + "RotGuess: " + rotGuess); 
		
	}
	
	public static void predictLocation(long stepsFromNow)
	{
		//initially it assumes no change

		
		Self.firingAt.x = Self.target.location.x+ (Self.gunCorrection*Self.target.velocity*Self.target.direction.x);
		Self.firingAt.y = Self.target.location.y+ (Self.gunCorrection*Self.target.velocity*Self.target.direction.y);
		
		if(Self.firingAt.x > Self.arenaWidth-18)Self.firingAt.x = Self.arenaWidth-18;
		else if(Self.firingAt.x <18) Self.firingAt.x = 18;
		if(Self.firingAt.y > Self.arenaHeight-18)Self.firingAt.y = Self.arenaHeight-18;
		else if(Self.firingAt.y <18) Self.firingAt.y = 18;
		
		Self.firingAt.updateMag();
		
	}
	
}
