package Garage;
import java.util.*;

import robocode.*;

import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
	
/**
 * WeAllAreRobertPaulson - a robot by (cIaoaI)
 */
public class Vector4d extends Vector
{
	public double x;
	public double y;
	public double z;
	public double g;
	public double mag;
	public boolean magCurrent;
	
	//constructors
	Vector4d()
	{
		length = 4;
		x = 0.0;
		y = 0.0;
		z = 0.0;
		g = 0.0;
		mag = 0.0;
		magCurrent = false;
	}
	
	Vector4d(double xi, double yi, double zi, double gi)
	{
		length = 4;
		x = xi;
		y = yi;
		z = zi;
		g = gi;
		mag = 0.0;
		magCurrent = false;
	}
	
	Vector4d(Vector4d v)
	{
		length = 4;
		x = v.x;
		y = v.y;
		z = v.z;
		g = v.g;
		mag = v.mag;
		magCurrent = v.magCurrent;
	}
	
	//operations
	public Vector4d plus(Vector4d v)
	{
		return new Vector4d(v.x+x, v.y+y, v.z+z, v.g+g);
	}
	
	public Vector4d minus(Vector4d v)
	{
		return new Vector4d(x - v.x, y - v.y, z - v.z, g - v.g);
	}
	
	public double dot(Vector4d v)
	{
		return x*v.x + y*v.y + z*v.z + g*v.g;
	}
	
	public void updateMag()
	{
		mag = Math.sqrt(x*x + y*y + z*z + g*g);
		magCurrent = true;
	}
	
	public Vector4d getNormalized()
	{
		if(!magCurrent)updateMag();
		
		return (mag != 0.0)? new Vector4d(x/mag, y/mag, z/mag, g/mag) : this;
	}
	
	public double[] getArray()
	{
		double[] ret = new double[length];
		ret[0] = x;
		ret[1] = y;
		ret[2] = z;
		ret[3] = g;
		return ret;
	}
	
	public void normalizeMe()
	{
		if(!magCurrent) updateMag();
 
		if(mag != 0.0)
		{
			x /= mag;
			y /= mag;
			z /= mag;
			g /= mag;
			mag = 1;
		}
	}
}
