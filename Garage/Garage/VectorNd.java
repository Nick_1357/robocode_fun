package Garage;
import robocode.*;

import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
	
/**
 * WeAllAreRobertPaulson - a robot by (cIaoaI)
 */
public class VectorNd extends Vector
{
	public VecTree variables;	//the vector's dimensional values
	public double mag;			//vector's magnitude
	public boolean magCurrent;	//flag to see if mag needs to be calculated
	
	public static VecTree dimKey = new VecTree();// maybe use this later to abstract the task of keeping track of which dimension has which index 
	
	/////////////////////////////////////////////constructors
	VectorNd()
	{
		length = 0;
		variables = new VecTree();
		mag = 0.0;
		magCurrent = false;
	}
	
	VectorNd(double[] vars, int[] mapping)//this is optimized when mapping is in top-down binary tree order
	{
		length = vars.length;
		variables = new VecTree();
		for(int i=0; i<length; i++)
		{
			variables.add( new Node(vars[i], mapping[i]) );
		}
		if(variables.needBallance)
		{
			variables.ballanceTree();
		}
		
		mag = 0.0;
		magCurrent = false;
	}
	
	VectorNd(VectorNd v)//expensive copy because of the tree
	{
		if(v == null)
		{
			length = 0;
			variables = new VecTree();
			mag = 0.0;
			magCurrent = false;
		}
		else
		{
			length = v.length;
			variables = new VecTree(v.variables);
			mag = v.mag;
			magCurrent = v.magCurrent;
		}
		
	}
	
	/////////////////////////////////////////////////////utility
	
	public double getDimValByIndex(int index)//returns 0 if the dimension is not in this vector
	{
		return variables.getNodeByIndex(index).dat;
	}
	
	/////////////////////////////////////////////////////operations
	
	public VectorNd plus(VectorNd v)//more efficient when v has more dimensions than caller ~~ kinda: unimportant differences when they are similar
	{
		//first, find the union of indices between the two vectors
		
		VectorNd ret = new VectorNd(this);//make a copy to be the union
		
		Node[] myVars = variables.getListOrder();//getListOrder() messes the tree up completely: 
		Node[] vVars = v.variables.getListOrder();// .ballanceTree() needs to be called to correct it
		
		for(int i=0, j=0; i< myVars.length; i++)
		{
			while(myVars[i].index > vVars[j].index)
			{
				ret.variables.add(vVars[j].dat, vVars[j].index);//add it to the union
				
				j++;
				if(j==vVars.length)
				{
					j--;
					break;
				}
			} 
			
			if(myVars[i].index == vVars[j].index)
			{
				ret.variables.add(myVars[i].dat+vVars[j].dat, myVars[i].index);//put the sum in the union: add() updates with to the new value when indices are the same
				
				j++;//move the j index up and keep going
			}
			//else: myVars[i].index must be less, so do nothing and move the i index up.
			
			//if there are more vVars after all myVars have been cycled, add them all to the union
			while(i==myVars.length-1 && j<vVars.length)
			{
				ret.variables.add(vVars[j].dat, vVars[j].index);
				j++;
			}
		}
		
		//ret is the sum of both vectors.  clean up and return it
		variables.ballanceTree();
		v.variables.ballanceTree();
		if(ret.variables.needBallance)ret.variables.ballanceTree();//take performance where luck gives it
		
		ret.magCurrent = false;
		ret.length = ret.variables.count;
		return ret;
	}
	
	public VectorNd minus(VectorNd v)//more efficient when v has more dimensions than caller ~~ kinda: unimportant differences when they are similar
	{
		//first, find the union of indices between the two vectors
		
		VectorNd ret = new VectorNd(this);//make a copy to be the union
		
		Node[] myVars = variables.getListOrder();//getListOrder() messes the tree up completely: 
		Node[] vVars = v.variables.getListOrder();// .ballanceTree() needs to be called to correct it
		
		for(int i=0, j=0; i< myVars.length; i++)
		{
			while(myVars[i].index > vVars[j].index)
			{
				ret.variables.add(-vVars[j].dat, vVars[j].index);// add -vVars[j] to the union
				
				j++;
				if(j==vVars.length)
				{
					j--;
					break;
				}
			} 
			
			if(myVars[i].index == vVars[j].index)
			{
				ret.variables.add(myVars[i].dat - vVars[j].dat, myVars[i].index);//put the sum in the union: add() updates with to the new value when indices are the same
				
				j++;//move the j index up and keep going
			}
			//else: myVars[i].index must be less, so do nothing and move the i index up.
			
			//if there are more vVars after all myVars have been cycled, add them all to the union
			while(i==myVars.length-1 && j<vVars.length)
			{
				ret.variables.add(-vVars[j].dat, vVars[j].index);
				j++;
			}
		}
		
		//ret is the sum of both vectors.  clean up and return it
		variables.ballanceTree();
		v.variables.ballanceTree();
		if(ret.variables.needBallance)ret.variables.ballanceTree();//take performance where luck gives it
		
		ret.magCurrent = false;
		ret.length = ret.variables.count;
		return ret;
	}
	
	public double dot(VectorNd v)//more efficient when v has more dimensions
	{
		int dot = 0;
		Node[] myVals = variables.getListOrder();
		for(int i=0; i<myVals.length; i++)
		{
			dot+= v.getDimValByIndex(myVals[i].index) * myVals[i].dat;
		}
		variables.ballanceTree();
		
		return dot;
	}
	
	public void updateMag()
	{
		if(magCurrent)return;
		Node[] myVals = variables.getListOrder();
		int sum = 0;
		
		for(int i=0; i<myVals.length; i++)
		{
			sum += myVals[i].dat*myVals[i].dat;
		}
		
		mag = Math.sqrt(sum);
		magCurrent = true;
		variables.ballanceTree();
	}
	
	public VectorNd getNormalized()
	{
		VectorNd ret = new VectorNd(this);
		ret.normalizeMe();
		return ret;
	}
	
	public double[] getArray()
	{
		double[] ret = new double[length];
		Node[] tmp = variables.getListOrder();
		
		for(int i=0; i<length; i++)
		{
			ret[i] = tmp[i].dat;
		}
		
		variables.ballanceTree();
		return ret;
	}
	
	public void normalizeMe()
	{
		Node[] myVals = variables.getListOrder();
		//if(!magCurrent)updateMag(); no reason to break the tree down and build it up twice
		if(!magCurrent)
		{
			int sum = 0;
			
			for(int i=0; i<myVals.length; i++)
			{
				sum += myVals[i].dat*myVals[i].dat;
			}
			
			mag = Math.sqrt(sum);
			magCurrent = true;
		}
		
		if(mag==0.0) return;
		
		for(int i=0; i< myVals.length; i++)
		{
			myVals[i].dat /= mag;
		}
		
		mag = 1;
		
	}
}
